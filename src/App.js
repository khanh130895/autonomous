import React, { useRef, useState, useEffect } from "react";
import Draggable from "./components/Draggable";

export const ColorContext = React.createContext("color");

const App = () => {
  const square = useRef(null);
  const circle = useRef(null);

  const is_colliding = function(square, circle) {
    const boundingQ = square.getBoundingClientRect();
    const boundingC = circle.getBoundingClientRect();

    var q_distFromTop = boundingQ.y + boundingQ.height;
    var q_distFromLeft = boundingQ.x + boundingQ.width;
    var c_distFromTop = boundingC.y + boundingC.height;
    var c_distFromLeft = boundingC.x + boundingC.width;

    var not_colliding =
      q_distFromTop < boundingC.y ||
      boundingQ.y > c_distFromTop ||
      q_distFromLeft < boundingC.x ||
      boundingQ.x > c_distFromLeft;
    return !not_colliding;
  };

  const [isColliding, setIsColliding] = useState(null);

  useEffect(() => {
    setIsColliding(is_colliding(square.current, circle.current));
  }, []);

  const handleOnDrag = () => {
    setIsColliding(is_colliding(square.current, circle.current));
  };

  const handleOnDragEnd = () => {
    setIsColliding(is_colliding(square.current, circle.current));
  };

  return (
    <div
      style={{
        height: "450px",
        overflow: "hidden",
        position: "relative",
        border: "1px dashed #000"
      }}
    >
      <ColorContext.Provider value={{ isColliding: isColliding }}>
        <Draggable
          ref={circle}
          position={{ x: 497, y: 297 }}
          radius={120}
          onDrag={handleOnDrag}
          onDragEnd={handleOnDragEnd}
        />
        <div
          data-testid="square"
          ref={square}
          id="square"
          style={{
            height: "400px",
            width: "400px",
            backgroundColor: "#97ccc8",
            position: "absolute",
            zIndex: 0,
            top: "50px",
            left: "50px",
            border: "1px dashed #000"
          }}
        />
      </ColorContext.Provider>
    </div>
  );
};

export default App;
