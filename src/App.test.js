import React from "react";
import { render, fireEvent } from "@testing-library/react";

import App from "./App";

describe("<App />", () => {
  it("render app with square and circle", () => {
    const { getByTestId } = render(<App />);
    expect(getByTestId("square")).toBeInTheDocument();
    expect(getByTestId("circle")).toBeInTheDocument();
  });

  it("check app contains draggable div", () => {
    const { getByTestId } = render(<App />);
    const draggableElement = getByTestId("circle");
    fireEvent.mouseDown(draggableElement);
    fireEvent.mouseMove(draggableElement, {
      clientX: 16,
      clientY: 16
    });
    fireEvent.mouseUp(draggableElement);
    expect(draggableElement).toHaveStyle("transform: translate(16px, 16px);");
  });
});
