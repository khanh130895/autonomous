import styled from "styled-components";

const Circle = styled.div.attrs(({ x, y, radius, isColliding }) => ({
  style: {
    transform: `translate(${x}px, ${y}px)`,
    backgroundColor: `${isColliding ? "#d08b8b" : "#6eca74"}`,
    width: `${radius * 2}px`,
    height: `${radius * 2}px`,
    zIndex: 1
  }
}))`
  cursor: grab;
  position: absolute;
  width: 250px;
  height: 250px;
  border-radius: 50%;

  ${({ isDragging }) =>
    isDragging &&
    `
    opacity: 0.8;
    cursor: grabbing;
  `}
`;

export default Circle;
