import React, { useState, useEffect, useCallback, forwardRef } from "react";
import { useContext } from "react";
import { ColorContext } from "../App";

import Circle from "./Circle";

const Draggable = ({ position, radius, onDrag, onDragEnd }, ref) => {
  const [state, setState] = useState({
    isDragging: false,
    translateX: position.x,
    translateY: position.y
  });

  const { isColliding } = useContext(ColorContext);

  // mouse move
  const handleMouseMove = useCallback(
    ({ clientX, clientY }) => {
      if (state.isDragging) {
        setState(prevState => ({
          ...prevState,
          translateX: clientX,
          translateY: clientY
        }));
        onDrag(state);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [state.isDragging]
  );

  // mouse left click release
  const handleMouseUp = useCallback(() => {
    if (state.isDragging) {
      setState(prevState => ({
        ...prevState,
        isDragging: false
      }));
      onDragEnd(state);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.isDragging]);

  // mouse left click hold
  const handleMouseDown = useCallback(() => {
    setState(prevState => ({
      ...prevState,
      isDragging: true
    }));
  }, []);

  useEffect(() => {
    window.addEventListener("mousemove", handleMouseMove);
    window.addEventListener("mouseup", handleMouseUp);

    return () => {
      window.removeEventListener("mousemove", handleMouseMove);
      window.removeEventListener("mouseup", handleMouseUp);
    };
  }, [handleMouseMove, handleMouseUp]);

  return (
    <Circle
      data-testid="circle"
      isDragging={state.isDragging}
      onMouseDown={handleMouseDown}
      radius={radius}
      x={state.translateX}
      y={state.translateY}
      ref={ref}
      isColliding={isColliding}
    />
  );
};

export default forwardRef(Draggable);
